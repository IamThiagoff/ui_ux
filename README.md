# React Haiku

# projeto

React Hooks & Utilities que economizam tempo e linhas de código

## Funções

| Hooks | Utilities |
| :---: | :---: | 
| useClipboard() | For |
| useHover() | If |
|useInputValue()|Show|
|useLeaveDetection()|
|useMediaQuery()|
|useMousePosition()|
|usePrefersTheme()|
|useScript()|
|useToggle()|
|useUrgentUpdate()|
